# Api.MoonHare

A mock API server for the Moon & Hare project.

## Development server

Run `npm run start-mockapi` to generate data and start an API server listening on `http://localhost:3001/`.

var schema = {
  type: 'object',
  properties: {
    perfumes: {
      type: 'array',
      minItems: 12,
      maxItems: 24,
      items: {
        type: 'object',
        properties: {
          id: {
            type: 'integer',
            unique: true,
            minimum: 1
          },
          name: {
            type: 'string',
            faker: 'commerce.productName'
          },
          brandName: {
            type: 'string',
            faker: 'company.companyName'
          },
          description: {
            type: 'string',
            faker: 'lorem.paragraph'
          },
          volume: {
            type: 'integer',
            minimum: 1,
            maximum: 5
          },
          scentNotes: {
            type: 'array',
            minItems: 1,
            maxItems: 15,
            items: {
              type: 'string',
              faker: 'scent.note'
            }
          },
          price: {
            type: 'number',
            faker: 'commerce.price'
          },
          image: {
            type: 'string',
            faker: 'scent.image'
          }
        },
        required: [
          'id',
          'name',
          'brandName',
          'description',
          'volume',
          'scentNotes',
          'price',
          'image'
        ]
      }
    }
  },
  required: ['perfumes']
};

module.exports = schema;

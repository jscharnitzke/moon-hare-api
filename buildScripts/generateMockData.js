var jsf = require('json-schema-faker');
var fs = require('fs');

jsf.extend('faker', () => {
  var faker = require('faker');

  faker.scent = {
    note: function() {
      var notes = [
        'oud',
        'rose',
        'juniper',
        'jasmine',
        'fir',
        'lavender',
        'ylang ylang',
        'cherry blossom',
        'bamboo',
        'apple',
        'strawberry',
        'cedar',
        'sandalwood',
        'incense',
        'chocolate',
        'vanilla',
        'orchid',
        'lemon',
        'lilac',
        'oakmoss',
        'spanish moss',
        'vetiver',
        'patchouli',
        'blueberry',
        'coconut',
        'rosemary',
        'basil',
        'mango',
        'musk',
        'red musk',
        'soil',
        'black musk',
        'loam',
        'frankincense',
        'myrrh',
        "dragon's blood resin",
        'white chocolate',
        'violet',
        'woodsmoke'
      ];

      return notes[Math.floor(Math.random() * notes.length)];
    },
    image: function() {
      return '/assets/product-placeholder.jpg';
    }
  };

  return faker;
});

var mockDataSchema = require('./mockDataSchema');

jsf.resolve(mockDataSchema).then(() => {
  var json = JSON.stringify(jsf(mockDataSchema));

  fs.writeFile('./src/api/db.json', json, function(err) {
    if (err) {
      return console.log(err);
    } else {
      console.log('Mock data generated.');
    }
  });
});
